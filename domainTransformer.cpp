#include "domainTransformer.h"
#include <stdlib.h>

////////////////////////////////////////////////////////////////////////////////////

fftw_complex* DomainTransformer::fftGet(double *signal, long int signalLen)
{
    fftw_complex * spectrum = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * signalLen);

    fftw_plan p = fftw_plan_dft_r2c_1d(signalLen, signal, spectrum, FFTW_ESTIMATE);

    fftw_execute(p);

    fftw_destroy_plan(p);

    return spectrum;
}

////////////////////////////////////////////////////////////////////////////////////

double* DomainTransformer::ifftGet(fftw_complex *spectrum, long int signalLen)
{
    double * signal = (double*) malloc(sizeof(double) * signalLen);

    fftw_plan p = fftw_plan_dft_c2r_1d(signalLen, spectrum, signal, FFTW_ESTIMATE);

    fftw_execute(p);

    fftw_destroy_plan(p);

    return signal;
}
