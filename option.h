#ifndef OPTION_H
#define OPTION_H

#include <stdio.h>
#include <iostream>
#include "fftw3.h"

class Option //factory
{
  public:
    static Option *chooseOption(int choice);
    virtual ~Option(){}
    virtual void start() = 0;
  protected:
    FILE* readFile(std::string instruction);
    FILE* writeFile(std::string instruction);
    int addVariable(std::string instruction, std::string reinstruct, int lowerLimit, int upperLimit);
};

class DisplaySignal: public Option
{
public:
    ~DisplaySignal(){}
    void start();
};

class SignalMod: public Option
{
public:
    ~SignalMod(){}
    void start();

private:
    void stereoToMono();
    void leftRightTransition();
    void crossfading(int samplingRate);
};

class SpectralAnalysis: public Option
{
public:
    ~SpectralAnalysis(){}
    void start();

private:
    void frequencyRemoval(int samplingRate);
    void frequencyShift(int samplingRate);
};

#endif // OPTION_H
