#ifndef MENU_H
#define MENU_H

#include<iostream>

class Menu //singleton
{
public:
    static Menu* getInstance();
    static void resetInstance();
    void intro();
    void selectOption();

private:
    static Menu* m_instance;
};


#endif // MENU_H
