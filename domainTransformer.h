#ifndef DOMAINTRANSFORMER_H
#define DOMAINTRANSFORMER_H

#include "fftw3.h"

class DomainTransformer
{
public:
    static fftw_complex* fftGet(double *signal, long int signalLen);
    static double* ifftGet(fftw_complex *spectrum, long int signalLen);
};


#endif // DOMAINTRANSFORMER_H
