#ifndef GRAPHWINDOW_H
#define GRAPHWINDOW_H

#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <string>

class GraphWindow //singleton
{
public:
    GraphWindow(int,int, std::string); ///call drawXX() immediately after or it crashes
    ~GraphWindow();
    void drawSignal(FILE*,int sampleRate,int frameFactor);

private:
    sf::RenderWindow* m_window;
};

#endif // GRAPHWINDOW_H
