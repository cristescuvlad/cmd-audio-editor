#include "menu.h"
#include "option.h"
#include <iostream>

Menu* Menu::getInstance()
{
    if(m_instance == NULL)
    {
        m_instance = new Menu;
    }

    return m_instance;
}

void Menu::resetInstance()
{
    std::cout<<"\n  La revedere!\n\n";
    delete m_instance;
    m_instance = NULL;
}

void Menu::intro()
{
    std::cout<<"\n\n  Bine ati venit! Acest program poate efectua operatii referitoare\nla modificarea semnalelor audio digitale.\n";
}

void Menu::selectOption()
{
    Option* optiune;
    int choice;

    while (true)
    {
        std::cout <<"\n -------------------------------------------------------\n";
        std::cout <<" -------------------------------------------------------\n";
        std::cout <<"\n  Selectati una dintre urmatoarele optiuni:\n\n";
        std::cout << "  (1) Reprezentarea grafica a semnalului\n  (2) Modificari ale semnalului\n  (3) Modificari ale spectrului de frecvente\n  (4) Iesire\n\n  Alegeti optiunea: ";
        std::cin >> choice;

        //implement check if int

        while( choice <= 0 || choice >= 5)
        {
            std::cout << " Optiune incorecta. Reintroduceti: ";
            std::cin >> choice;
        }

        if( choice == 4)
            return ;

        optiune = Option::chooseOption(choice);
        optiune -> start();

        delete optiune;
    }
}
