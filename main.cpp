#include <iostream>
#include <conio.h>
#include "menu.h"
#include "option.h"

Menu* Menu::m_instance=NULL; //initialize Menu singleton

int main()
{
    Menu *menu=Menu::getInstance();
    menu->intro();
    menu->selectOption();

    Menu::resetInstance();
    delete menu;

    return 0;
}
