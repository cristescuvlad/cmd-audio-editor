#include "graphWindow.h"
#include <iostream>
#include <time.h>
#include <math.h>
#include <SFML/Audio.hpp>

#define TRANSLATION 5000
#define AMPLITUDE 0.15

#define WWIDTH 400
#define WHEIGHT 800
#define WZOOM 30
#define MWHEELRATE 0.1

GraphWindow::GraphWindow(int length, int height, std::string title)
{
    m_window = new sf::RenderWindow(sf::VideoMode(length, height), title);
}

GraphWindow::~GraphWindow()
{
    delete m_window;
}

void GraphWindow::drawSignal(FILE* input, int sampleRate, int frameFactor)
{
    std::cout<<"\n Comenzi optionale:\n P - pauza/redare\n A - salt inapoi de 3 secunde\n D - salt inainte de 3 secunde\n F - salt inapoi de 30 secunde\n G - salt inainte de 30 secunde\n";
    std::cout<<"\n Atentie! Selectati fereastra grafica inainte de inserarea comenzilor. Tragerea ferestrei pune pauza redarii.\n\n";

    fseek(input,0,SEEK_END);
    int filesize = ftell(input);
    fseek(input,0,SEEK_SET);

    int fps = sampleRate/frameFactor;

    short bufl1=0,bufr1=0,bufl2,bufr2;

    /// audio

    sf::SoundBuffer buffer;
    sf::Sound sound;
    short uintBuf[fps*2]={0};
    sound.setBuffer(buffer);

    ///window / view

    sf::View view;
    view.reset(sf::FloatRect(0, 0, WWIDTH, WHEIGHT));

    view.zoom(WZOOM);
    m_window->setView(view);

    ///

    bool pause = 0;

    while(m_window->isOpen())
    {
        buffer.loadFromSamples(&uintBuf[0],fps*sizeof(short),2,sampleRate);
        sound.play();

        clock_t time = clock();  //begin counting passing milliseconds

        for(int j=0;j<fps;j++)
        {
            if(!feof(input))
            {
                fread(&bufl2,sizeof(short),1,input);   //read pcm for left channel
                uintBuf[2*j]=bufl2;

                fread(&bufr2,sizeof(short),1,input);   //read pcm for right channel
                uintBuf[2*j+1]=bufr2;
            }

            sf::Vertex lineL[] =        //draw lines composing the signal
            {
                sf::Vertex(sf::Vector2f(float(j-fps), float(bufl1*AMPLITUDE))),
                sf::Vertex(sf::Vector2f(float(j+1-fps), float(bufl2*AMPLITUDE)))
            };

            sf::Vertex lineR[] =
            {
                sf::Vertex(sf::Vector2f(float(j+fps/4), float(bufr1*AMPLITUDE))),
                sf::Vertex(sf::Vector2f(float(j+1+fps/4), float(bufr2*AMPLITUDE)))
            };

            m_window->draw(lineL, 2, sf::Lines);
            m_window->draw(lineR, 2, sf::Lines);

            bufl1=bufl2;
            bufr1=bufr2;
        }

        m_window->display();

        time=clock()-time; //elapsed time

        sf::sleep(sf::milliseconds(1000/frameFactor-1000*float(time)/CLOCKS_PER_SEC));  ///sleep (frame in milliseconds - time of calculation)

        m_window->clear();

        sf::Event event;

        while (m_window->pollEvent(event) || pause)
        {
            if (event.type == sf::Event::Closed)
            {
                pause=0;
                m_window->close();
            }

            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::A)
            {
                if((long int)ftell(input) > (long int)(3*sampleRate*2*sizeof(short)))  //careful not to seek out of file
                    fseek(input,-3*sampleRate*2*sizeof(short),SEEK_CUR);  // 2 channels and 2 bytes (short)
                else
                    fseek(input,0,SEEK_SET);
            }

            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::D)
            {
                if((long int)(ftell(input)+3*sampleRate*2*sizeof(short)) < (long int)filesize)  //careful not to seek out of file
                    fseek(input,3*sampleRate*2*sizeof(short),SEEK_CUR);  // 2 channels and 2 bytes (short)
                else
                    fseek(input,-sampleRate*2*sizeof(short),SEEK_END);
            }

            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::F)
            {
                if((long int)(ftell(input)) > (long int)(30*sampleRate*2*sizeof(short)))  //careful not to seek out of file
                    fseek(input,-30*sampleRate*2*sizeof(short),SEEK_CUR);  // 2 channels and 2 bytes (short)
                else
                    fseek(input,0,SEEK_SET);
            }

            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::G)
            {
                if((long int)(ftell(input)+30*sampleRate*2*sizeof(short)) < (long int)filesize)  //careful not to seek out of file
                    fseek(input,30*sampleRate*2*sizeof(short),SEEK_CUR);  // 2 channels and 2 bytes (short)
                else
                    fseek(input,-sampleRate*2*sizeof(short),SEEK_END);
            }

            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::P)
            {
                pause = !pause;
            }

            if (event.type == sf::Event::MouseWheelMoved)
            {
                view.zoom(1-event.mouseWheel.delta*MWHEELRATE);
                m_window->setView(view);
            }
        }
    }
}
