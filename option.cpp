#include <iostream>
#include <math.h> //sin, cos, min
#include <stdio.h>
#include <sstream> //convert numbers to strings
#include "option.h"
#include "graphWindow.h"
#include "domainTransformer.h"

#include <conio.h>

Option *Option::chooseOption(int choice)
{
    switch(choice)
    {
    case 1:
        return new DisplaySignal;
    case 2:
        return new SignalMod;
    case 3:
        return new SpectralAnalysis;
    default:
        return NULL;
    }
}

////////////////////////////////////////////////////////////////////////////////////

FILE* Option::readFile(std::string instruction)
{
    //returns file stream or null if failed

    std::string path;
    FILE * input = new FILE;

    do
    {
        std::cout<<instruction;     //implement extension check
        std::cin>>path;
        input = fopen(path.c_str(),"rb");
        if(!input)
        {
             std::cout<<" Eroare la citirea fisierului.\n"<<" Reintroduceti calea catre fisier? (Y/N): ";
             std::string opt;
             std::cin>>opt;
             while(opt != "y" && opt != "Y" && opt != "n" && opt != "N")
             {
                std::cout<<" Reintroduceti optiunea (Y/N): ";
                std::cin>>opt;
             }
             if(opt == "n" || opt == "N")
             {
                 fclose(input);
                 delete input;
                 return NULL;
             }
        }
    }
    while(!input);

    return input;
}

///////////////////////////////////////////////////////////////////////////////////

FILE* Option::writeFile(std::string instruction)
{
    std::cout<<instruction;
    std::string outputPath;

    std::cin>>outputPath;

    FILE * output = fopen(outputPath.c_str(),"wb");

    return output;
}

///////////////////////////////////////////////////////////////////////////////////

int Option::addVariable(std::string instruction, std::string reinstruct, int lowerLimit, int upperLimit)
{
    int var;

    std::cout<<instruction;
    std::cin>>var;

    while(var<lowerLimit || var > upperLimit)
    {
        std::cout<<reinstruct;
        std::cin>>var;
    }

    return var;

}

///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

void DisplaySignal::start()
{
    FILE* input = readFile("\n  Introduceti calea catre fisierul audio.\n  Atentie, fisierul audio trebuie sa fie stereo, sa aiba extensia .pcm,\n rata de esantionare 44100 Hz si adancimea 16 biti.\n\n Formatul de citire: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

    if(input)
    {
        int xDim=800,yDim=600;

        GraphWindow* window = new GraphWindow(xDim,yDim,"Reprezentarea grafica a semnalului"); ///call window->drawXX() immediately after or it crashes

        window->drawSignal(input,44100,10);

        delete window;

        fclose(input);
    }
}

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

void SignalMod::start()
{
    unsigned int opt = 1;
    while(opt)
    {
        std::cout<<"\n ----------------------------------------------------------------------- ";
        std::cout<<"\n  Optiuni: \n  (1) Transformare din stereo in mono\n  (2) Tranzitie stanga-dreapta\n  (3) Tranzitia dintre 2 fisiere\n  (4) Iesire\n\n  Introduceti optiunea: ";

        char optC;
        std::cin>>optC;

        while(optC<'1' || optC>'4')
        {
            std::cout<<"\n  Optiune gresita. Reintroduceti optiunea: ";
            std::cin>>optC;
        }

        opt = optC-'0'; //convert from char to int

        if(opt==4)
        {
            return ;
        }

        if(opt==1)
        {
            stereoToMono();
        }
        if(opt==2)
        {
            leftRightTransition();
        }
        if(opt==3)
        {
            crossfading(44100);
        }
    }

}

////////////////////////////////////////////////////////////////////////////////////

void SignalMod::stereoToMono()
{
    FILE * input = readFile("\n  \n  Introduceti calea catre fisierul audio.\n  Atentie, fisierul audio trebuie sa fie stereo,\n sa aiba extensia .pcm si adancimea 16 biti.\n\n Formatul de citire: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

    if(input)
    {
        FILE * output = writeFile("\n Introduceti calea dorita pentru locatia fisierului nou format.\n Formatul caii: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

        if(output)
        {
            std::cout<<"\n Va rog, asteptati.";

            short bufL,bufR;

            while(!feof(input))
            {
                fread(&bufL,sizeof(short),1,input);
                fread(&bufR,sizeof(short),1,input);

                bufL=(bufL+bufR)/2;

                fwrite(&bufL,sizeof(short),1,output);
            }

            fclose(output);

            std::cout<<"\n Gata!\n";
        }

        fclose(input);
    }
}

////////////////////////////////////////////////////////////////////////////////////

void SignalMod::leftRightTransition()
{
    FILE * input = readFile("\n  \n  Introduceti calea catre fisierul audio.\n  Atentie, fisierul audio trebuie sa fie stereo,\n sa aiba extensia .pcm si adancimea 16 biti.\n\n Formatul de citire: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

    if(input)
    {
        FILE * output = writeFile("\n Introduceti calea dorita pentru locatia fisierului nou format.\n Formatul caii: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

        if(output)
        {


            int speed = addVariable(
                "\n Introduceti viteza de alternare a canalului (1 - foarte incet, 100 - foarte repede): ",
                "\n Introduceti o viteza de alternare intre 1 si 100: ",
                1,
                100
                );

            std::cout<<"\n Va rog, asteptati.";

            short bufL,bufR;

            unsigned int iteration = 0;

            while(!feof(input))
            {
                fread(&bufL,sizeof(short),1,input);
                fread(&bufR,sizeof(short),1,input);

                bufL = short(double(bufL)*sin(iteration*0.000001*speed)); //hardcoded
                bufR = short(double(bufR)*cos(iteration*0.000001*speed));

                fwrite(&bufL,sizeof(short),1,output);
                fwrite(&bufR,sizeof(short),1,output);

                iteration++;
            }

            fclose(output);

            std::cout<<"\n Gata!\n";
        }

        fclose(input);

    }
}

////////////////////////////////////////////////////////////////////////////////////

void SignalMod::crossfading(int samplingRate)
{
    FILE * input1 = readFile("\n  Introduceti calea catre primul fisier audio.\n  Atentie, fisierul audio trebuie sa fie stereo, sa aiba extensia .pcm,\n rata de esantionare 44100 Hz si adancimea 16 biti.\n\n Formatul de citire: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

    if(input1)
    {
        FILE * input2 = readFile("\n  Introduceti calea catre al 2-lea fisier audio.\n  Atentie, fisierul audio trebuie sa fie stereo, sa aiba extensia .pcm,\n rata de esantionare 44100 Hz si adancimea 16 biti.\n\n Formatul de citire: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

        if(input2)
        {
            FILE * output = writeFile("\n Introduceti calea dorita pentru locatia fisierului nou format.\n Formatul caii: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

            if(output)
            {
                //get lengths of the files
                fseek(input1,0,SEEK_END);
                unsigned int nrOfSamples1 = ftell(input1)/sizeof(short);
                fseek(input1,0,SEEK_SET);

                fseek(input2,0,SEEK_END);
                unsigned int nrOfSamples2 = ftell(input2)/sizeof(short);
                fseek(input2,0,SEEK_SET);

                std::stringstream ss;
                ss<<std::min(nrOfSamples1/(2*samplingRate),nrOfSamples2/(2*samplingRate));

                unsigned int cfDur = addVariable(
                        "\n Introduceti numarul de secunde in care doriti sa se faca tranzitia\n (1 - " + ss.str() + "): ",
                        "\n Reintroduceti numarul de secunde (1 - " + ss.str() + "): ",
                        1,
                        std::min(nrOfSamples1/(2*samplingRate),nrOfSamples2/(2*samplingRate)) //2 = stereo
                        );

                unsigned int cfType = addVariable(
                        "\n Introduceti tipul de tranzitie\n  (1) Liniar\n  (2) Exponential\n  (3) Logaritmic\n  (4) Iesire\n  Introduceti optiunea: ",
                        "\n Introduceti una dintre optiunile de mai sus (1-4): ",
                        1,
                        4
                        );

                if(cfType != 4)
                {
                    std::cout<<"\n Va rog, asteptati.";

                    short buf1, buf2;   //1 - first file , 2 - second file


                    //copy the first part of the file
                    for(unsigned long int i=0;i<nrOfSamples1/2 - samplingRate*cfDur;i++)
                    {
                        fread(&buf1,sizeof(short),1,input1);
                        fread(&buf2,sizeof(short),1,input1);

                        fwrite(&buf1,sizeof(short),1,output);
                        fwrite(&buf2,sizeof(short),1,output);
                    }

                    //do the crossfade

                    for(unsigned long int i=0;i<2*samplingRate*cfDur;i++)  // 2 = nr of channels
                    {
                        fread(&buf1,sizeof(short),1,input1);
                        fread(&buf2,sizeof(short),1,input2);

                        double norm_i = (double(i)+1.0)/double(2.0*samplingRate*cfDur);  //normed i


                        if(cfType == 1)  //linear
                        {
                            buf1=short(
                                    double(buf1)*(1-norm_i)
                                          +
                                    double(buf2)*(norm_i)
                                    );
                        }
                        if(cfType == 2)   //exp
                        {
                            buf1=short(
                                    double(buf1)*(1-((exp(norm_i)-1)/(exp(1)-1)))
                                       +
                                    double(buf2)*(exp(norm_i)-1)/(exp(1)-1)
                                    );
                        }
                        if(cfType == 3)    //log
                        {
                            buf1=short(
                                   double(buf1)*double(
                                            1.0-log(1.0-norm_i)/log(1.0/double(2.0*samplingRate*cfDur))
                                                       )
                                       +
                                    double(buf2)*double(
                                            log(1.0-norm_i) / log( 1.0/double(2.0*samplingRate*cfDur))
                                                        )
                                   );
                        }

                        fwrite(&buf1,sizeof(short),1,output);
                    }

                    //copy the rest

                    while(!feof(input2))
                    {
                        fread(&buf2,sizeof(short),1,input2);
                        fwrite(&buf2,sizeof(short),1,output);
                    }

                    std::cout<<"\n\nGata!\n\n";

                }//if(cfType !=4 )

                fclose(output);
            }//if(output)

            fclose(input2);
        }//if(input2)

        fclose(input1);
    }//if(input1)
}

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

void SpectralAnalysis::start()
{
    int opt = 1;
    while(opt)
    {
        std::cout<<"\n ----------------------------------------------------------------------- ";
        std::cout<<"\n  Optiuni: \n  (1) Eliminarea frecventelor\n  (2) Deplasarea frecventelor\n  (3) Iesire\n\n  Introduceti optiunea: ";

        char optC;
        std::cin>>optC;

        while(optC<'1' || optC>'3')
        {
            std::cout<<"\n  Optiune gresita. Reintroduceti optiunea: ";
            std::cin>>optC;
        }

        opt = optC-'0';  //convert from char to int

        if(opt==3)
        {
            return ;
        }

        if(opt==1)
        {
            frequencyRemoval(44100);
        }
        if(opt==2)
        {
            frequencyShift(44100);
        }
    }

}

////////////////////////////////////////////////////////////////////////////////////

void SpectralAnalysis::frequencyRemoval(int samplingRate)
{
    FILE* input = readFile("\n \n  Introduceti calea catre fisierul audio.\n  Atentie, fisierul audio trebuie sa fie stereo, sa aiba extensia .pcm,\n rata de esantionare 44100 Hz si adancimea 16 biti.\n\n Formatul de citire: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

    if(input)
    {
        FILE * output = writeFile("\n Introduceti calea dorita pentru locatia fisierului nou format.\n\n Formatul caii: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

        if(output)
        {
            std::vector<double> leftSignal;
            std::vector<double> rightSignal;

            //get user frequency

            std::stringstream ss;
            ss<<samplingRate/2;

            int lowFreq = addVariable(
                                      std::string("\n Introduceti limita de jos a intervalului de frecvente\n pe care doriti sa le stergeti (numar intreg intre 0 si " + ss.str() +"): "),
                                      "\n Frecventa incorecta. Reintroduceti frecventa minima: ",
                                      0,
                                      samplingRate/2
                                      );
            int highFreq = addVariable(
                                       std::string("\n Introduceti limita de sus a intervalului de frecvente\n pe care doriti sa le stergeti (numar intreg intre 0 si " + ss.str() +"): "),
                                       "\n Frecventa incorecta. Reintroduceti frecventa minima: ",
                                       0,
                                       samplingRate/2
                                       );

            std::cout<<"\n Va rog asteptati\n\n";

            while(!feof(input))
            {
                unsigned int sampleInd = 0;  //index of samples (1 channel)

                while(!feof(input) && sampleInd < (unsigned int)samplingRate) //process one second at the time
                {
                    short bufL,bufR;

                    fread(&bufL,sizeof(short),1,input);
                    leftSignal.push_back((double)bufL);

                    fread(&bufR,sizeof(short),1,input);
                    rightSignal.push_back((double)bufR);

                    sampleInd++;
                }

                unsigned int signalSize = (long int)leftSignal.size();

                //get spectrum

                fftw_complex* spectrumL = DomainTransformer::fftGet(&leftSignal[0],signalSize);
                leftSignal.clear();

                fftw_complex* spectrumR = DomainTransformer::fftGet(&rightSignal[0],signalSize);
                rightSignal.clear();

                //get signal frequency

                double freq[signalSize];

                for(unsigned int i=0;i<signalSize;i++)
                {
                    freq[i]=(double)i*(double)samplingRate/(double)signalSize;
                }

                //remove selected frequencies

                for(unsigned int i=1;i<signalSize;i++)
                {
                    if(
                         (freq[i] > (double)lowFreq && freq[i] < (double)highFreq) ||
                         (freq[i] < (double)signalSize-(double)lowFreq && freq[i] > (double)signalSize-(double)highFreq)
                       )
                    {
                        spectrumL[i][0]=0; spectrumL[i][1]=0;
                        spectrumR[i][0]=0; spectrumR[i][1]=0;
                    }
                }

                //reverse signal

                double* modLeftSignal = DomainTransformer::ifftGet(spectrumL,signalSize);
                fftw_free(spectrumL);
                double* modRightSignal = DomainTransformer::ifftGet(spectrumR,signalSize);
                fftw_free(spectrumR);

                //output to file

                short buf;
                double *left = modLeftSignal;
                double *right = modRightSignal;

                for(unsigned int i=0;i<signalSize;i++)
                {
                    buf=(short)(*left/(double)signalSize);
                    fwrite(&buf,sizeof(short),1,output);
                    buf=(short)(*right/(double)signalSize);
                    fwrite(&buf,sizeof(short),1,output);

                    left++;
                    right++;
                }

                free(modLeftSignal); free(modRightSignal);
            }//while(!feof(input))

            std::cout<<"\nGata!\n";

            fclose(output);
        }//if(output)

        fclose(input);
    }//if(input)
}

////////////////////////////////////////////////////////////////////////////////////

void SpectralAnalysis::frequencyShift(int samplingRate)
{
    FILE* input = readFile("\n \n  Introduceti calea catre fisierul audio.\n  Atentie, fisierul audio trebuie sa fie stereo, sa aiba extensia .pcm,\n rata de esantionare 44100 Hz si adancimea 16 biti.\n\n Formatul de citire: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

    if(input)
    {
        FILE * output = writeFile("\n Introduceti calea dorita pentru locatia fisierului nou format.\n\n Formatul caii: DRIVE:\\Folder\\...\\Folder\\fisier.pcm:\n");

        if(output)
        {
            std::vector<double> leftSignal;
            std::vector<double> rightSignal;

            //get user shift

            std::stringstream ss;
            ss<<samplingRate/2;

            int translation = addVariable(std::string("\n Introduceti treapta de deplasare a frecventelor\n (numar intreg intre -" + ss.str() + " si " + ss.str() +"): "),
                                          "\n Numar incorect. Reintroduceti treapta: ",
                                          -samplingRate/2,
                                          samplingRate/2
                                          );


            std::cout<<"\n Va rog asteptati.\n\n";

            while(!feof(input))
            {
                unsigned int sampleInd = 0;  //index of samples (1 channel)

                while(!feof(input) && sampleInd < (unsigned int)samplingRate) //process one second at the time
                {
                    short bufL,bufR;

                    fread(&bufL,sizeof(short),1,input);
                    leftSignal.push_back((double)bufL);

                    fread(&bufR,sizeof(short),1,input);
                    rightSignal.push_back((double)bufR);

                    sampleInd++;
                }

                unsigned int signalSize = (long int)leftSignal.size();

                //get spectrum

                fftw_complex* spectrumL = DomainTransformer::fftGet(&leftSignal[0],signalSize);
                leftSignal.clear();

                fftw_complex* spectrumR = DomainTransformer::fftGet(&rightSignal[0],signalSize);
                rightSignal.clear();

                //shift frequencies

                if (translation > 0 && translation < (int)signalSize/2)
                {

                    for (unsigned int i = 0; i < signalSize / 2 - translation; i++)
                    {
                        spectrumL[signalSize / 2 + i][0] = spectrumL[signalSize / 2 + i + translation][0];
                        spectrumL[signalSize / 2 + i][1] = spectrumL[signalSize / 2 + i + translation][1];
                        spectrumL[signalSize / 2 - i][0] = spectrumL[signalSize / 2 - i - translation][0];
                        spectrumL[signalSize / 2 - i][1] = spectrumL[signalSize / 2 - i - translation][1];

                        spectrumR[signalSize / 2 + i][0] = spectrumR[signalSize / 2 + i + translation][0];
                        spectrumR[signalSize / 2 + i][1] = spectrumR[signalSize / 2 + i + translation][1];
                        spectrumR[signalSize / 2 - i][0] = spectrumL[signalSize / 2 - i - translation][0];
                        spectrumR[signalSize / 2 - i][1] = spectrumL[signalSize / 2 - i - translation][1];
                    }

                    for (unsigned int i = signalSize / 2 - translation; i < signalSize / 2; i++)
                    {
                        spectrumL[signalSize / 2 + i][0] = 0;
                        spectrumL[signalSize / 2 + i][1] = 0;
                        spectrumL[signalSize / 2 - i][0] = 0;
                        spectrumL[signalSize / 2 - i][1] = 0;

                        spectrumR[signalSize / 2 + i][0] = 0;
                        spectrumR[signalSize / 2 + i][1] = 0;
                        spectrumR[signalSize / 2 - i][0] = 0;
                        spectrumR[signalSize / 2 - i][1] = 0;
                    }
                }

                if (translation < 0 && translation > -(int)signalSize/2)
                {

                    for (unsigned int i = 1; i < signalSize / 2 + translation; i++)
                    {
                        spectrumL[i][0] = spectrumL[i - translation][0];
                        spectrumL[i][1] = spectrumL[i - translation][1];
                        spectrumL[signalSize - i -1][0] = spectrumL[i][0];
                        spectrumL[signalSize - i -1][1] = spectrumL[i][1];

                        spectrumR[i][0] = spectrumR[i - translation][0];
                        spectrumR[i][1] = spectrumR[i - translation][1];
                        spectrumR[signalSize - i -1][0] = spectrumR[i][0];
                        spectrumR[signalSize - i -1][1] = spectrumR[i][1];
                    }

                    for (unsigned int i = signalSize / 2 + translation; i < signalSize / 2 - 1; i++)
                    {
                        spectrumL[i][0] = 0;
                        spectrumL[i][1] = 0;
                        spectrumL[signalSize - i - 1][0] = 0;
                        spectrumL[signalSize - i - 1][1] = 0;

                        spectrumR[i][0] = 0;
                        spectrumR[i][1] = 0;
                        spectrumR[signalSize - i - 1][0] = 0;
                        spectrumR[signalSize - i - 1][1] = 0;
                    }
                }

                if (translation <= -(int)signalSize / 2 || translation >= (int)signalSize / 2)
                {
                    for (unsigned int i = 1; i < signalSize - 1; i++)
                    {
                        spectrumL[i][0] = 0;
                        spectrumL[i][1] = 0;
                        spectrumR[i][0] = 0;
                        spectrumR[i][1] = 0;
                    }
                }

                //reverse signal

                double* modLeftSignal = DomainTransformer::ifftGet(spectrumL,signalSize);
                fftw_free(spectrumL);
                double* modRightSignal = DomainTransformer::ifftGet(spectrumR,signalSize);
                fftw_free(spectrumR);

                //output to file

                short buf;
                double *left = modLeftSignal;
                double *right = modRightSignal;

                for(unsigned int i=0;i<signalSize;i++)
                {
                    buf=(short)(*left/(double)signalSize);
                    fwrite(&buf,sizeof(short),1,output);
                    buf=(short)(*right/(double)signalSize);
                    fwrite(&buf,sizeof(short),1,output);

                    left++;
                    right++;
                }

                free(modLeftSignal); free(modRightSignal);
            }//while(!feof(input))

            std::cout<<"\nGata!\n";
            fclose(output);
        }//if(output)

        fclose(input);
    }//if(input)
}

